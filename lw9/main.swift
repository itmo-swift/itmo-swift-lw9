//
//  main.swift
//  lw9
//
//  Created by Daniil Lushnikov on 28.09.2021.
//  Sort array without mutating functions

// n - amount of elemets in array
let n = Int(readLine() ?? "") ?? 0
// fill array with elements
var array: [Int] = Array(repeating: 0, count: n)
for i in 0..<n {
    array[i] = Int(readLine() ?? "") ?? 0
}
// print array (sorted and default)
for elem in array.sorted(by: <) {
    print(elem)
}
for elem in array {
    print(elem)
}
